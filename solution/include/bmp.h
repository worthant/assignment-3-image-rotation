#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

/**
 * @brief BMP File Header Structure.
 *
 * Represents the header of a BMP (Bitmap Image File Format) file.
 * Used for reading and writing information about a BMP image, such as dimensions,
 * color depth, and compression metadata. The structure is aligned to a 1-byte
 * boundary to match the exact data structure of a BMP file.
 *
 * @note Use #pragma pack(push, 1) before declaring the structure and #pragma pack(pop)
 * after to control the alignment of the structure in memory.
 */
#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;             ///< Identifier, 'BM' for Bitmap. Typically 'BM' (0x4D42).
    uint32_t bfileSize;          ///< Total file size in bytes.
    uint32_t bfReserved;         ///< Reserved; must be set to 0.
    uint32_t bOffBits;           ///< Offset to the start of image data.
    uint32_t biSize;             ///< Size of the BITMAPINFOHEADER. Should be 40 for BMP.
    uint32_t biWidth;            ///< Image width in pixels.
    uint32_t biHeight;           ///< Image height in pixels.
    uint16_t biPlanes;           ///< Number of color planes. Should be 1.
    uint16_t biBitCount;         ///< Number of bits per pixel. Typically 24 (for 24-bit BMP).
    uint32_t biCompression;      ///< Compression type. 0 for no compression.
    uint32_t biSizeImage;        ///< Image size in bytes. Can be 0 for uncompressed images.
    uint32_t biXPelsPerMeter;    ///< Horizontal resolution in pixels per meter. 0 if unspecified.
    uint32_t biYPelsPerMeter;    ///< Vertical resolution in pixels per meter. 0 if unspecified.
    uint32_t biClrUsed;          ///< Number of colors used in the palette. 0 for 24-bit images.
    uint32_t biClrImportant;     ///< Number of important colors. 0 if all colors are important.
};
#pragma pack(pop)


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, const struct image* img);

#endif


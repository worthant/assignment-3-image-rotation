#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

/**
 * @struct pixel
 * @brief Represents a single pixel in an RGB color space.
 *
 * This structure defines the basic building block for an image.
 * It consists of three components, each represented by an 8-bit unsigned integer.
 * These components are the red, green, and blue (RGB) values of the pixel.
 */
struct pixel {
    uint8_t b; ///< Blue component of the pixel.
    uint8_t g; ///< Green component of the pixel.
    uint8_t r; ///< Red component of the pixel.
};

/**
 * @struct image
 * @brief Represents an image as a 2D array of pixels.
 *
 * This structure defines an image in terms of its width and height (in pixels),
 * and a pointer to an array of pixel structures that hold the image data.
 * The image is represented in row-major order, where each row of pixels
 * is placed in consecutive memory locations.
 */
struct image {
    uint64_t width;   ///< Width of the image in pixels.
    uint64_t height;  ///< Height of the image in pixels.
    struct pixel* data; ///< Pointer to the array of pixels representing the image data.
};

/**
 * @brief Creates an image of specified width and height.
 *
 * This function allocates memory for a new image of the given dimensions.
 * It initializes the width and height of the image and allocates memory for
 * the pixel data. The function returns the created image.
 *
 * @param width The width of the image to be created.
 * @param height The height of the image to be created.
 * @return A struct image representing the newly created image.
 */
struct image create_img(uint64_t width, uint64_t height);

/**
 * @brief Destroys an image and frees its resources.
 *
 * This function deallocates the memory used for the image's pixel data
 * and sets the data pointer to NULL. It should be called to avoid memory leaks
 * when an image is no longer needed.
 *
 * @param img A pointer to the image structure to be destroyed.
 */
void destroy_img(struct image* img);

#endif // IMAGE_H

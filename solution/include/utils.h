#ifndef UTIL_H
#define UTIL_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

/**
 * @brief Validates the rotation angle.
 *
 * Checks if the given angle is one of the allowed values for rotation.
 * The valid angles are 0, 90, -90, 180, -180, 270, and -270 degrees.
 *
 * @param angle The angle to be validated.
 * @return 1 if the angle is valid, 0 otherwise.
 */
int validate_angle(int angle);


/**
 * @brief Opens a file with the specified mode.
 *
 * Attempts to open a file with the given filename and mode (e.g., "r" for read,
 * "w" for write). If the file cannot be opened, an error message is printed
 * and the program may exit depending on the implementation.
 *
 * @param filename The name of the file to be opened.
 * @param mode The mode in which the file should be opened.
 * @return A FILE pointer to the opened file, or NULL if the file couldn't be opened.
 */
FILE* open_file(const char* filename, const char* mode);


/**
 * @brief Closes an opened file.
 *
 * This function closes a file that was previously opened with open_file or
 * any other file opening function. It ensures that any buffer associated
 * with the file is properly flushed and the file is closed. This is a
 * necessary step to avoid resource leaks.
 *
 * @param file A pointer to the FILE object that needs to be closed. If the
 * file pointer is NULL, the function has no effect.
 */
void close_file(FILE* file);


/**
 * @brief Reads an image from BMP file
 * 
 * This function opens BMP file, reads its content and converts it into the
 * internal image format. It handles the opening, reading, and closing of the
 * file, as well as the conversion from BMP format to the internal image 
 * representation. The function utilizes error handling to ensure proper file
 * operations. In case of an error, the program may exit.
 *
 * @param filename The name of the BMP file to be read.
 * @return A struct image containing the pixel data and dimensions of the image.
 */
struct image read_image_from_file(const char* filename);


/**
 * @brief Writes an image to a BMP file.
 *
 * This function takes an image in the internal format and writes it to a BMP file.
 * The function handles the creation, writing, and closing of the file, ensuring
 * that the image is properly encoded in the BMP format. It manages error checking
 * and will exit the program in case of a failure during file operations.
 *
 * @param filename The name of the BMP file where the image will be written.
 * @param img A pointer to the image structure that holds the image to be written.
 */
void write_image_to_file(const char* filename, const struct image* img);


/**
 * @brief Copies pixel data from source to destination.
 *
 * @param dest Pointer to the destination pixel array.
 * @param src Pointer to the source pixel array.
 * @param count Number of pixels to copy.
 */
void copy_pixels(struct pixel* dest, const struct pixel* src, size_t count);


/**
 * @brief Sets a range of bytes to zero, typically used for padding.
 *
 * @param start Pointer to the start of the byte range.
 * @param count Number of bytes to set to zero.
 */
void set_padding(uint8_t* start, size_t count);

#endif

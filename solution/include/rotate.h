#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"


/**
 * @brief Rotates an image by a specified angle.
 *
 * This function rotates the given image by the specified angle. The angle
 * should be one of the following: 0, 90, -90, 180, -180, 270, -270 degrees.
 * Positive angles represent clockwise rotation, while negative angles
 * represent counterclockwise rotation.
 *
 * @param source The source image to be rotated.
 * @param angle The angle of rotation in degrees.
 * @return A new image struct containing the rotated image. The original
 * image remains unchanged.
 */
struct image rotate_img(struct image const source, int angle);

#endif // ROTATE_H

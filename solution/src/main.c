#include "../include/image.h"
#include "../include/rotate.h"
#include "../include/utils.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    int angle = atoi(argv[3]);
    if (!validate_angle(angle)) {
        fprintf(stderr, "Invalid angle.\n");
        return 1;
    }

    struct image img = read_image_from_file(argv[1]);
    struct image rotated_img = rotate_img(img, angle);
    destroy_img(&img);

    write_image_to_file(argv[2], &rotated_img);
    destroy_img(&rotated_img);

    printf("Image successfully transformed and saved.\n");
    return 0;
}

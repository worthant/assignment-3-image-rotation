#include "../include/bmp.h"
#include "../include/utils.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int validate_angle(int angle) {
    int valid_angles[] = { 0, 90, -90, 180, -180, 270, -270 };
    for (int i = 0; i < sizeof(valid_angles) / sizeof(valid_angles[0]); i++) {
        if (angle == valid_angles[i]) {
            return 1; // Valid angle
        }
    }
    return 0; // Invalid angle
}

FILE* open_file(const char* filename, const char* mode) {
    FILE* file = fopen(filename, mode);
    if (!file) {
        perror("Error opening file");
    }
    return file;
}

void close_file(FILE* file) {
    if (file) {
        if (fclose(file) != 0) {
            perror("Error closing file");
        }
    }
}

struct image read_image_from_file(const char* filename) {
    FILE* input_file = open_file(filename, "rb");
    if (!input_file) {
        exit(1);
    }

    struct image img;
    enum read_status status = from_bmp(input_file, &img);
    close_file(input_file);

    if (status != READ_OK) {
        exit(1);
    }

    return img;
}

void write_image_to_file(const char* filename, const struct image* img) {
    FILE* output_file = open_file(filename, "wb");
    if (!output_file) {
        exit(1);
    }

    enum write_status status = to_bmp(output_file, img);
    close_file(output_file);

    if (status != WRITE_OK) {
        exit(1);
    }
}

void copy_pixels(struct pixel* dest, const struct pixel* src, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        dest[i] = src[i];
    }
}

void set_padding(uint8_t* start, size_t count) {
    for (size_t i = 0; i < count; ++i) {
        start[i] = 0;
    }
}

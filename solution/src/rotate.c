#include "../include/image.h"
#include "../include/rotate.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

static struct image rotate_90_degrees(struct image const source) {
    struct image rotated = create_img(source.height, source.width);
    for (uint64_t y = 0; y < source.height; ++y) {
        for (uint64_t x = 0; x < source.width; ++x) {
            rotated.data[(rotated.height - 1 - x) * rotated.width + y] = source.data[y * source.width + x]; // Corrected for clockwise rotation
        }
    }
    return rotated;
}

static struct image rotate_180_degrees(struct image const source) {
    struct image rotated = create_img(source.width, source.height);
    for (uint64_t y = 0; y < source.height; ++y) {
        for (uint64_t x = 0; x < source.width; ++x) {
            rotated.data[(rotated.height - 1 - y) * rotated.width + (rotated.width - 1 - x)] = source.data[y * source.width + x];
        }
    }
    return rotated;
}

struct image rotate_img(struct image const source, int angle) {
    struct image rotated;
    struct image temp;

    switch (angle) {
    case 90:
    case -270:
        rotated = rotate_90_degrees(source);
        break;
    case 180:
    case -180:
        rotated = rotate_180_degrees(source);
        break;
    case 270:
    case -90:
        // Achieve 270 degrees rotation by first rotating 180 degrees, then 90 degrees
        temp = rotate_180_degrees(source);
        rotated = rotate_90_degrees(temp);
        destroy_img(&temp);
        break;
    default: // 0 degree rotation (or invalid angle)
        rotated = create_img(source.width, source.height);
        for (uint64_t i = 0; i < source.width * source.height; i++) {
            rotated.data[i] = source.data[i];
        }
        break;
    }
    return rotated;
}

#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image create_img(uint64_t width, uint64_t height) {
    struct image img = {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel)) // Allocate memory for pixels
    };

    if (img.data == NULL) {
        fprintf(stderr, "Failed to allocate memory for image data\n");
        exit(1); // Exit if memory allocation fails
    }

    return img;
}

void destroy_img(struct image* img) {
    if (img->data != NULL) {
        free(img->data);
        img->data = NULL;
    }
}
